﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    public GameObject secretTextObject;

    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;

    public GameObject spawnablePrefab; // adds the Spawnable Prefab tab in the component menu for the Player.

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;

        SetCountText();
        winTextObject.SetActive(false); // while the game is playing and until the player has obtained all x amount of collectibles, this will remain off.
        secretTextObject.SetActive(false); // the secret text stays off until it's ready.
    }


    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();

        if (count >= 40) // This can be raised indefinitely, but around the 180 - 200 mark it becomes unplayable, as the ball becomes way too big for the camera to render.
        {
            winTextObject.SetActive(true);
        }

        if (count >= 111)
        {
            secretTextObject.SetActive(true); // Makes the secret text appear!
        }
    }


    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed); // Main driving force of the sphere. This is what gives the sphere movement/force.
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1; // For each collectable picked up, this will raise the count by one. Could theoretically raise this to 2, 3, 4, and so on.

            SetCountText();

            transform.localScale += new Vector3(.1f, .1f, .1f); // Increases the size of the ball by .1 each time a pickup is collected. Take note that it will also inadvertedly affect the weight of the ball, making it easier to fling off.
            float randomRange = 8f; // Range of how far these cubes/pickups will go. 
            Vector3 randomPosition = new Vector3(Random.Range(-randomRange, randomRange), spawnablePrefab.transform.position.y, Random.Range(-randomRange, randomRange));
            Instantiate(spawnablePrefab, randomPosition, spawnablePrefab.transform.rotation);
            // Instantiate = spawn... word. Codeword for spawning in Unity. Gotta remember this.
            // Question is, are there other codewords, maybe shorter to use for spawning stuff? Probably not.
            // Note to self: try not to raise the randomRange float value too high, otherwise things could start spawning off the platform.
        }
    }
}
